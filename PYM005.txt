* Zones re�ues ou renvoy�es au programme appelant
RECEVOIR PCODACT PCODRET PCODCLI
* Initialisation de variables
PCODRET = *BLANK
* Appel du bloc INITIALISATION 01
INITIALISER 01
* Une erreur a �t� d�tect�e lors de l'initialisation
SI PCODRET <> *BLANK
   * Sortie du programme et retour au pgm appelant
   TERMINER
FIN
* Affiche l'�cran 01
TRAITER 01
XNOM_PGM = *NOM_PGM
XUSER = *USER
* Test si code action 'MO' ou 'VI'
SI PCODACT = 'MO';'VI';'SU'
   * Lecture du client demand�
   ZCCOD_CLIENT = PCODCLI
   SI PCODACT = 'MO';'SU'
      * Lecture du Client avec verrouillage
      LIRE VUE_CLIENTL
   SINON
      * Lecture du Client sans verrouillage
      LIRE VUE_CLIENTL *UNLCK
   FIN
   * Test si le client demand� existe
   SI VUE_CLIENTL EXISTE
      SI *BLOQUE = '1'
         PCODRET = 'VR'
      FIN
      * Affectation des zones �cran par les zones fichier
      ZCNOM_CLIENT = CNOM_CLIENT
      ZCADR_01_CLI = CADR_01_CLI
      ZZ_ADR_02_CLI = CADR_02_CLI
      ZCBUR_DIS_CLI = CBUR_DIS_CLI
      ZCCOD_POS_CLI = CCOD_POS_CLI
      ZCNUM_TEL_CLI = CNUM_TEL_CLI
      ZCCOD_REGLEMT = CCOD_REGLEMT
      ZCMHT_CAF_CLI = CMHT_CAF_CLI
   SINON
      PCODRET = 'LE'
   FIN
FIN
* Protection/d�protection des zones maquette
TRAITER_PROC PROTECT
*
SI PCODACT = 'MO' ET (*F03 OU *F12)
   * D�verrouillage enregistrement
*    LIBERER VUE_CLIENTL
FIN
SI *F03 
   PCODRET = '03'
   TERMINER
FIN
*
SI *F12
   PCODRET = '12'
   TERMINER
FIN
* Si Visu on sort du programme
SI PCODACT = 'VI'
   TERMINER
FIN
* Contr�le les donn�es saisies � l'�cran
VERIFIER 01
* Mise � jour de la BD
VALIDER 01
* Sortie du programme pour retour au pgm appelant
TERMINER
* Controle si action CR
SI PCODACT = 'CR'
   * Lecture du fichier CLIENT sans verrouillage
   LIRE VUE_CLIENTL *UNLCK
   * Si le code client existe d�j� => erreur
   SI VUE_CLIENTL EXISTE
      PREPARER_MSG CLI0001 ZCCOD_CLIENT
      INIT_MSG ZCCOD_CLIENT
      ERREUR
   FIN
FIN
* Le nom client doit �tre saisi
SI ZCNOM_CLIENT = *BLANK
   PREPARER_MSG 0008 ZCNOM_CLIENT
   ERREUR
FIN

* Affectation des zones �cran dans les zones fichier
CCOD_CLIENT = ZCCOD_CLIENT
CNOM_CLIENT = ZCNOM_CLIENT
CADR_01_CLI = ZCADR_01_CLI
CADR_02_CLI = ZZ_ADR_02_CLI
CBUR_DIS_CLI = ZCBUR_DIS_CLI
CCOD_POS_CLI = ZCCOD_POS_CLI
CNUM_TEL_CLI = ZCNUM_TEL_CLI
CCOD_REGLEMT = ZCCOD_REGLEMT
CMHT_CAF_CLI = ZCMHT_CAF_CLI
* R�cup�ration du nom de l'utilisateur courant
CUSR_DER_MAJ = *USER
* R�cup�ration de la date courante du travail
CDAT_DER_MAJ = *DATE8
CDAT_DER_MAJ = &FORMAT_DATE8(CDAT_DER_MAJ;'DMY';'YMD')
* R�cup�ration de la date courante
HEURE WDATE
CDAT_DER_MAJ = &DATE_NUM(WDATE)
*
HEURE WTIMSTP
*
SI PCODACT = 'MO'
   * Mise � jour de l'enreg du fichier CLIENT
   METTRE_A_JOUR VUE_CLIENTL
FIN
*
SI PCODACT = 'CR'
   * Cr�ation du fichier CLIENT
   CREER VUE_CLIENTL
FIN

* Gestion de la protection des zones suivant la valeur du code action
SI PCODACT = 'MO';'VI';'SU'   
   * Protection de la zone code client
   PROTEGER *VRAI ZCCOD_CLIENT 01
   SOULIGNER *FAUX ZCCOD_CLIENT 01
   * Protection des autres zones
   SI PCODACT = 'VI'
      PROTEGER *VRAI ZCNOM_CLIENT ZCADR_01_CLI -
         ZZ_ADR_02_CLI ZCBUR_DIS_CLI ZCCOD_POS_CLI -
         ZCCOD_REGLEMT ZCMHT_CAF_CLI ZCNUM_TEL_CLI 01
      SOULIGNER *FAUX ZCNOM_CLIENT ZCADR_01_CLI -
         ZZ_ADR_02_CLI ZCBUR_DIS_CLI ZCCOD_POS_CLI -
         ZCCOD_REGLEMT ZCMHT_CAF_CLI ZCNUM_TEL_CLI 01
   SINON
      PROTEGER *FAUX ZCNOM_CLIENT ZCADR_01_CLI -
         ZZ_ADR_02_CLI ZCBUR_DIS_CLI ZCCOD_POS_CLI -
         ZCCOD_REGLEMT ZCMHT_CAF_CLI ZCNUM_TEL_CLI 01
      SOULIGNER *VRAI ZCNOM_CLIENT ZCADR_01_CLI -
         ZZ_ADR_02_CLI ZCBUR_DIS_CLI ZCCOD_POS_CLI -
         ZCCOD_REGLEMT ZCMHT_CAF_CLI ZCNUM_TEL_CLI 01
   FIN
SINON
   * La zone code client doit �tre saisissable
   PROTEGER *FAUX ZCCOD_CLIENT 01
   SOULIGNER *VRAI ZCCOD_CLIENT 01
   SOULIGNER *VRAI ZCNOM_CLIENT ZCADR_01_CLI -
    ZZ_ADR_02_CLI ZCBUR_DIS_CLI ZCCOD_POS_CLI -
    ZCCOD_REGLEMT ZCMHT_CAF_CLI ZCNUM_TEL_CLI 01
FIN
